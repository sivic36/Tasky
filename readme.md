# RMA Homework project

## LV3 - Tasky

**Tasky** - Android app for organising users tasks

<img src="screenshots/Tasky_logo.png" title="Tasky logo" width="100" height="100">

### The assignment and problems encountered

App works with local database in wich saves tasks created by
user. User can add new task, delete it from database, set category and priority for his tasks and create new categories.

Beside working with database, app should implement RecyclerView for showing list of users tasks.

### Utilised snippets/solutions/libraries

* **RecyclerView**

```gradle
implementation 'com.android.support:recyclerview-v7:26.1.0'
```

* CardView

```gradle
implementation 'com.android.support:cardview-v7:26.1.0'
```

* ButterKnife

```gradle
implementation 'com.jakewharton:butterknife:8.8.1'
annotationProcessor 'com.jakewharton:butterknife-compiler:8.8.1'
```

* **Room** components - for easier accessing and managing database  and data persistence

```gradle
    implementation "android.arch.persistence.room:runtime:$rootProject.roomVersion"
    annotationProcessor "android.arch.persistence.room:compiler:$rootProject.roomVersion"
    androidTestImplementation "android.arch.persistence.room:testing:$rootProject.roomVersion"
```

* Lifecycle components - for common tasks like lifecycle management

```gradle
implementation "android.arch.lifecycle:extensions:$rootProject.archLifecycleVersion"
annotationProcessor "android.arch.lifecycle:compiler:$rootProject.archLifecycleVersion"
```

 * Material Spinner - stylized Material Design spinner

 ```gradle
implementation 'com.jaredrummler:material-spinner:1.2.5'
```

Used example for implementing Android Architecture Components including Room library:
* [Android Codelab](https://codelabs.developers.google.com/codelabs/android-room-with-a-view/index.html?index=..%2F..%2Findex#0)

![Portrait view of MainActivity when button is clicked](https://codelabs.developers.google.com/codelabs/android-room-with-a-view/img/3840395bfb3980b8.png)

### Screenshots

<img src="screenshots/screen1.png" title="Welcome screen" height="350">
<img src="screenshots/screen2.png" title="ListActivity - showing all tasks" height="350">
<img src="screenshots/screen3.png" title="NewTaskActivity" height="350">

<img src="screenshots/screen4.png" title="New category" height="350">
<img src="screenshots/screen5.png" title="Categories spinner" height="350">
<img src="screenshots/screen6.png" title="Alert delete category" height="350">

<img src="screenshots/preview.gif" title="Preview" height="400">

### Key points

* **Inserting a new task**

```java
// Starting NewTaskActivity for result (new task)
public void addNewTask(){
    Intent intent = new Intent(getApplicationContext(), NewTaskActivity.class);
    startActivityForResult(intent, NEW_TASK_ACTIVITY_REQUEST_CODE);
}

...

// Getting result - new task from NewTaskActivity
public void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);

    if (requestCode == NEW_TASK_ACTIVITY_REQUEST_CODE && resultCode == RESULT_OK) {
        Task task = new Task(
                data.getStringExtra(NewTaskActivity.CATEGORY),
                data.getIntExtra(NewTaskActivity.PRIORITY_MARK, 0),
                data.getStringExtra(NewTaskActivity.TASK));
        mTaskViewModel.insert(task);
    }
}
```

* **Deleting a task**

User can easily delete a task with *swipe-to-delete* gesture. Using created class RecyclerItemTouchHelper which extends ItemTouchHelper.SimpleCallback and with one swipe to the *left*, the *swiped* task will be deleted from the database (and automatically from the list shown on the UI).

```java
ItemTouchHelper.SimpleCallback itemTouchHelperCallback =
        new RecyclerItemTouchHelper(0, ItemTouchHelper.LEFT, this);
new ItemTouchHelper(itemTouchHelperCallback).attachToRecyclerView(this.rvTasksList);

...

// Deleting a task (swiped item)
    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction, int position) {
        if (viewHolder instanceof TaskAdapter.ViewHolder) {
            // remove the task from db
            mTaskViewModel.delete(taskAdapter.getItem(position));
            Snackbar.make(viewHolder.itemView, "Task deleted!", Snackbar.LENGTH_SHORT).show();
        }
    }
```

* **Creating a new category and deleting a category**

When user clicks on **ADD NEW** category button, a simple dialog with *EditText* element appears where user can type name of the new category; that string is used for creating and inserting a new category in database:

```java
btnAddNewCategory.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        // Check
        if (TextUtils.isEmpty(etCategory.getText())) {
            Toast.makeText(getApplicationContext(), "Please name your category!", Toast.LENGTH_SHORT).show();
        }
        // Adding new category in db and closing the Dialog
        else {
            mTaskViewModel.insertCategory(new Category(etCategory.getText().toString()));
            spinnerCtg.setItems(categoryList);
            newCategoryDialog.dismiss();
        }
    }
});
```

When user wants to delete a selected category from spinner which holds list of all categories, an alert dialog shows to check user response; if positive, selected category is deleted from database:

```java
public void onClick(DialogInterface dialog, int which) {
    // continue with delete
    if(categoryList.size()>1)
        mTaskViewModel.deleteCategory(new Category(categoryList.get(spinnerCtg.getSelectedIndex())));
}
```

### Comment

All the work with database - inserting, deleting and showing tasks and categories are done using ***TaskViewModel*** which communitaces between the **UI** and ***TaskRepository***, wich communicates with the database using *data acces object* - ***TaskDao***.
