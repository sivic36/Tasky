package hr.ferit.slavenivic.tasky.swipeHelper;

import android.support.v7.widget.RecyclerView;

/**
 * Created by Ivic on 4/18/2018.
 */

public interface RecyclerItemTouchHelperListener {
    void onSwiped(RecyclerView.ViewHolder viewHolder, int direction, int position);
}
