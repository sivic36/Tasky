package hr.ferit.slavenivic.tasky.room;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.ArrayList;
import java.util.List;

import hr.ferit.slavenivic.tasky.model.Category;
import hr.ferit.slavenivic.tasky.model.Task;

/**
 * Created by Ivic on 4/18/2018.
 */
@Dao
public interface TaskDao {

    // FOR TASKS
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Task task);

    @Delete
    void delete(Task task);

    @Query("SELECT * FROM task_table")
    LiveData<List<Task>> getAllTasks();

    @Query("DELETE FROM task_table")
    void deleteAll();

    // FOR CATEGORIES
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertCategory(Category category);

    @Delete
    void deleteCategory(Category category);

    @Query("SELECT * FROM category_table")
    LiveData<List<Category>> getAllCategories();

}
