package hr.ferit.slavenivic.tasky;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;

import java.util.ArrayList;
import java.util.List;

import hr.ferit.slavenivic.tasky.model.Category;
import hr.ferit.slavenivic.tasky.model.Task;
import hr.ferit.slavenivic.tasky.room.TaskDao;
import hr.ferit.slavenivic.tasky.room.TaskRoomDatabase;

/**
 * Created by Ivic on 4/18/2018.
 */

public class TaskRepository {
    private TaskDao taskDao;
    private LiveData<List<Task>> tasks;
    private LiveData<List<Category>> categories;

    public TaskRepository(Application application) {
        TaskRoomDatabase db = TaskRoomDatabase.getDatabase(application);
        taskDao = db.taskDao();
        tasks = taskDao.getAllTasks();
        categories = taskDao.getAllCategories();
    }

    public LiveData<List<Task>> getAllTasks() {
        return tasks;
    }
    public LiveData<List<Category>> getAllCategories() {
        return categories;
    }

    public void insert (Task task) {
        new insertAsyncTask(taskDao).execute(task);
    }
    public void delete (Task task) {
        new deleteAsyncTask(taskDao).execute(task);
    }

    public void insertCategory (Category category) {
        new insertCategoryAsyncTask(taskDao).execute(category);
    }
    public void deleteCategory (Category category) {
        new deleteCategoryAsyncTask(taskDao).execute(category);
    }

    private static class insertAsyncTask extends AsyncTask<Task, Void, Void> {

        private TaskDao mAsyncTaskDao;

        insertAsyncTask(TaskDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final Task... params) {
            mAsyncTaskDao.insert(params[0]);
            return null;
        }
    }
    private static class deleteAsyncTask extends AsyncTask<Task, Void, Void> {

        private TaskDao mAsyncTaskDao;

        deleteAsyncTask(TaskDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final Task... params) {
            mAsyncTaskDao.delete(params[0]);
            return null;
        }
    }

    private static class insertCategoryAsyncTask extends AsyncTask<Category, Void, Void> {

        private TaskDao mAsyncTaskDao;

        insertCategoryAsyncTask(TaskDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final Category... params) {
            mAsyncTaskDao.insertCategory(params[0]);
            return null;
        }
    }
    private static class deleteCategoryAsyncTask extends AsyncTask<Category, Void, Void> {

        private TaskDao mAsyncTaskDao;

        deleteCategoryAsyncTask(TaskDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final Category... params) {
            mAsyncTaskDao.deleteCategory(params[0]);
            return null;
        }
    }
}
