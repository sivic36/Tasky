package hr.ferit.slavenivic.tasky.activities;

import android.app.AlertDialog;
import android.app.Dialog;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.jaredrummler.materialspinner.MaterialSpinner;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import hr.ferit.slavenivic.tasky.R;
import hr.ferit.slavenivic.tasky.model.Category;
import hr.ferit.slavenivic.tasky.viewModel.TaskViewModel;

public class NewTaskActivity extends AppCompatActivity {

    public static final String PRIORITY_MARK = "priority";
    public static final String TASK = "task";
    public static final String CATEGORY = "category";

    @BindView(R.id.priority1) TextView priority1;
    @BindView(R.id.priority2) TextView priority2;
    @BindView(R.id.priority3) TextView priority3;
    @BindView(R.id.spinner_ctg) MaterialSpinner spinnerCtg;
    @BindView(R.id.btn_ctg) Button btnCtg;
    @BindView(R.id.et_task) EditText et_task;

    private int priority;
    private String category;
    ArrayList<String> categoryList;

    private TaskViewModel mTaskViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_task);
        ButterKnife.bind(this);

        //Default priority
        priority = R.color.colorPriority1;

        // Getting the viewModel
        mTaskViewModel = ViewModelProviders.of(this).get(TaskViewModel.class);

        // Setting spinner category list from db
        mTaskViewModel.getAllCategories().observe(this, new Observer<List<Category>>() {
            @Override
            public void onChanged(@Nullable List<Category> categories) {
                categoryList = new ArrayList<>();
                for(Category category : categories){
                    categoryList.add(category.getCategory());
                }
                spinnerCtg.setItems(categoryList);
                category = categories.get(spinnerCtg.getSelectedIndex()).getCategory();
            }
        });

        spinnerCtg.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener<String>() {
            @Override public void onItemSelected(MaterialSpinner view, int position, long id, String item) {
                category = item;
            }
        });

    }

    @OnClick({R.id.priority1, R.id.priority2, R.id.priority3, R.id.btn_ctg, R.id.btn_ctg_del, R.id.btn_add_task})
    public void userClick(View v){
        switch (v.getId()){
            // Changing priority
            case R.id.priority1:
                priority1.setBackground(getResources().getDrawable(R.drawable.bg_priority1));
                priority2.setBackground(getResources().getDrawable(R.drawable.bg_priority_none));
                priority3.setBackground(getResources().getDrawable(R.drawable.bg_priority_none));
                priority=R.color.colorPriority1;
                break;
            case R.id.priority2:
                priority2.setBackground(getResources().getDrawable(R.drawable.bg_priority2));
                priority1.setBackground(getResources().getDrawable(R.drawable.bg_priority_none));
                priority3.setBackground(getResources().getDrawable(R.drawable.bg_priority_none));
                priority=R.color.colorPriority2;
                break;
            case R.id.priority3:
                priority3.setBackground(getResources().getDrawable(R.drawable.bg_priority3));
                priority2.setBackground(getResources().getDrawable(R.drawable.bg_priority_none));
                priority1.setBackground(getResources().getDrawable(R.drawable.bg_priority_none));
                priority=R.color.colorPriority3;
                break;

            // Adding a new category
            case R.id.btn_ctg:
                // Creating a dialog
                final Dialog newCategoryDialog = new Dialog(this);
                newCategoryDialog.setContentView(R.layout.new_ctg_dialog);
                final EditText etCategory = newCategoryDialog.findViewById(R.id.et_category);
                Button btnAddNewCategory = newCategoryDialog.findViewById(R.id.btn_add_category);
                newCategoryDialog.show();

                btnAddNewCategory.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // Check
                        if (TextUtils.isEmpty(etCategory.getText())) {
                            Toast.makeText(getApplicationContext(), "Please name your category!", Toast.LENGTH_SHORT).show();
                        }
                        // Adding new category in db and closing the Dialog
                        else {
                            mTaskViewModel.insertCategory(new Category(etCategory.getText().toString()));
                            spinnerCtg.setItems(categoryList);
                            newCategoryDialog.dismiss();
                        }
                    }
                });
                break;

            // Deleting a category
            case R.id.btn_ctg_del:
                AlertDialog.Builder builder;
                builder = new AlertDialog.Builder(this, android.R.style.Theme_Material_Dialog_Alert);
                builder.setTitle("Delete entry")
                        .setMessage("Are you sure you want to delete this category?")
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // continue with delete
                                if(categoryList.size()>1)
                                    mTaskViewModel.deleteCategory(new Category(categoryList.get(spinnerCtg.getSelectedIndex())));
                            }
                        })
                        .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // do nothing
                            }
                        })
                        .setIcon(R.drawable.ic_dialog)
                        .setTitle("Delete category")
                        .show();
                break;

            // Adding new task
            case R.id.btn_add_task:
                Intent resultIntent = new Intent();
                // Check
                if (TextUtils.isEmpty(et_task.getText())) {
                    Toast.makeText(getApplicationContext(), "You forgot to write your task!", Toast.LENGTH_SHORT).show();
                }
                // Sending task data by Extras
                else {
                    String task = et_task.getText().toString();
                    resultIntent.putExtra(TASK, task);
                    resultIntent.putExtra(PRIORITY_MARK, priority);
                    resultIntent.putExtra(CATEGORY, category);
                    setResult(RESULT_OK, resultIntent);
                    finish();
                }
                break;
        }
    }
}
