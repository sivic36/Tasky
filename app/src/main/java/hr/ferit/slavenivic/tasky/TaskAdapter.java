package hr.ferit.slavenivic.tasky;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

import hr.ferit.slavenivic.tasky.model.Task;

/**
 * Created by Ivic on 4/17/2018.
 */

public class TaskAdapter extends RecyclerView.Adapter<TaskAdapter.ViewHolder> {

    List<Task> taskList;
    Context context;

    public TaskAdapter(Context context) {
        this.context = context;
    }

    public void setTasks(List<Task> taskList){
        this.taskList = taskList;
        notifyDataSetChanged();
    }

    @Override
    public TaskAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View taskView = inflater.inflate(R.layout.item_task, parent, false);
        return new ViewHolder(taskView);
    }

    @Override
    public void onBindViewHolder(TaskAdapter.ViewHolder holder, int position) {
        if(taskList!=null){
            Task task = this.taskList.get(position);
            holder.tvCategory.setText(task.getCategory());
            holder.tvTask.setText(task.getTask());
            holder.imPriority.setImageResource(task.getPriorityMark());
            //holder.viewBackground.setBackgroundResource(task.getPriorityMark());
        }
        else{
            //holder.tvTask.setText(R.string.no_tasks_msg);
        }

    }
    @Override
    public int getItemCount() {
        if(taskList != null)
        return this.taskList.size();
        else return 0;
    }

    public Task getItem(int position) {
        return taskList.get(position);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tvCategory, tvTask;
        public ImageView imPriority;
        public RelativeLayout viewBackground, viewForeground;
        public ViewHolder(View itemView) {
            super(itemView);
            this.tvCategory = itemView.findViewById(R.id.category_text);
            this.tvTask = itemView.findViewById(R.id.task);
            this.imPriority = itemView.findViewById(R.id.priority_mark);
            this.viewBackground = itemView.findViewById(R.id.background);
            this.viewForeground = itemView.findViewById(R.id.foreground);
        }
    }
}
