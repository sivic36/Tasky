package hr.ferit.slavenivic.tasky.activities;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.View;

import java.util.List;

import hr.ferit.slavenivic.tasky.R;
import hr.ferit.slavenivic.tasky.TaskAdapter;
import hr.ferit.slavenivic.tasky.activities.WelcomeActivity;
import hr.ferit.slavenivic.tasky.swipeHelper.RecyclerItemTouchHelper;
import hr.ferit.slavenivic.tasky.swipeHelper.RecyclerItemTouchHelperListener;
import hr.ferit.slavenivic.tasky.model.Task;
import hr.ferit.slavenivic.tasky.activities.NewTaskActivity;
import hr.ferit.slavenivic.tasky.viewModel.TaskViewModel;

public class ListActivity extends AppCompatActivity implements RecyclerItemTouchHelperListener {

    RecyclerView rvTasksList;
    TaskAdapter taskAdapter;
    RecyclerView.LayoutManager mLayoutManager;
    private TaskViewModel mTaskViewModel;

    public static final int NEW_TASK_ACTIVITY_REQUEST_CODE = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //mTaskViewModel.insert(new Task("Family", R.color.colorPriority2, "Pick up Mark from school"));
                //mLayoutManager.smoothScrollToPosition(rvTasksList,null,0);
                //Snackbar.make(view, "New task added!", Snackbar.LENGTH_SHORT).show();
                addNewTask();
            }
        });

        setUpUI();

        mTaskViewModel = ViewModelProviders.of(this).get(TaskViewModel.class);

        mTaskViewModel.getAllTasks().observe(this, new Observer<List<Task>>() {
            @Override
            public void onChanged(@Nullable final List<Task> tasks) {
                taskAdapter.setTasks(tasks);
            }
        });

        // Check if the 'New task' from WelcomeActivity is clicked when starting this activity
        boolean f = getIntent().getBooleanExtra(WelcomeActivity.NEW_TASK_FLAG, false);
        if(f)addNewTask();
    }

    private void setUpUI() {

        Context context = getApplicationContext();
        this.rvTasksList = this.findViewById(R.id.rv_tasks);
        this.taskAdapter = new TaskAdapter(context);
        this.mLayoutManager = new LinearLayoutManager(context);
        this.rvTasksList.setLayoutManager(this.mLayoutManager);
        this.rvTasksList.setAdapter(this.taskAdapter);
        this.rvTasksList.setItemAnimator(new DefaultItemAnimator());

        // For swipe-to-delete
        ItemTouchHelper.SimpleCallback itemTouchHelperCallback =
                new RecyclerItemTouchHelper(0, ItemTouchHelper.LEFT, this);
        new ItemTouchHelper(itemTouchHelperCallback).attachToRecyclerView(this.rvTasksList);
    }

    // Starting NewTaskActivity for result (new task)
    public void addNewTask(){
        Intent intent = new Intent(getApplicationContext(), NewTaskActivity.class);
        startActivityForResult(intent, NEW_TASK_ACTIVITY_REQUEST_CODE);
    }

    // Deleting a task (swiped item)
    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction, int position) {
        if (viewHolder instanceof TaskAdapter.ViewHolder) {
            // remove the task from db
            mTaskViewModel.delete(taskAdapter.getItem(position));
            Snackbar.make(viewHolder.itemView, "Task deleted!", Snackbar.LENGTH_SHORT).show();
        }
    }

    // Getting result - new task from NewTaskActivity
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == NEW_TASK_ACTIVITY_REQUEST_CODE && resultCode == RESULT_OK) {
            Task task = new Task(
                    data.getStringExtra(NewTaskActivity.CATEGORY),
                    data.getIntExtra(NewTaskActivity.PRIORITY_MARK, 0),
                    data.getStringExtra(NewTaskActivity.TASK));
            mTaskViewModel.insert(task);
        }
    }

}
