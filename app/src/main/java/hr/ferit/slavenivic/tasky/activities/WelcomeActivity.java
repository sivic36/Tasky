package hr.ferit.slavenivic.tasky.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import hr.ferit.slavenivic.tasky.R;

public class WelcomeActivity extends AppCompatActivity {

    public static final String NEW_TASK_FLAG = "startNewTask";
    @BindView(R.id.btn_new) Button btn_new;
    @BindView(R.id.btn_tasks) Button btn_tasks;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
        ButterKnife.bind(this);
    }
    @OnClick(R.id.btn_new)
    public void newTask(){
        taskActivity(true);
    }
    @OnClick(R.id.btn_tasks)
    public void showTasks(){
        taskActivity(false);
    }

    public void taskActivity(boolean newTask){
        // newTask - if true NewTaskActivity is started from ListActivity
        Intent intent = new Intent(getApplicationContext(), ListActivity.class);
        intent.putExtra(NEW_TASK_FLAG, newTask);
        startActivity(intent);
    }
}
