package hr.ferit.slavenivic.tasky.model;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

/**
 * Created by Ivic on 4/17/2018.
 */
@Entity(tableName="category_table")
public class Category {

    @PrimaryKey
    @NonNull
    private String category;

    public Category(@NonNull String category) {
        this.category = category;
    }

    @NonNull
    public String getCategory() {
        return category;
    }

    public void setCategory(@NonNull String category) {
        this.category = category;
    }
}