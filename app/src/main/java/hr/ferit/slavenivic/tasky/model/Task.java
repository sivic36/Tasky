package hr.ferit.slavenivic.tasky.model;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

/**
 * Created by Ivic on 4/17/2018.
 */
@Entity(tableName="task_table")
public class Task {

    @PrimaryKey
    @NonNull
    private String task;

    private String category;

    @ColumnInfo(name="priority")
    private int priorityMark;


    public Task(String category, int priorityMark, String task){
        this.category = category;
        this.priorityMark = priorityMark;
        this.task=task;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public int getPriorityMark() {
        return priorityMark;
    }

    public void setPriorityMark(int priorityMark) {
        this.priorityMark = priorityMark;
    }

    public String getTask() {
        return task;
    }

    public void setTask(String task) {
        this.task = task;
    }

}
