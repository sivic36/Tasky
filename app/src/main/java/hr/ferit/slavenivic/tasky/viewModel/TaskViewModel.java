package hr.ferit.slavenivic.tasky.viewModel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;

import java.util.ArrayList;
import java.util.List;

import hr.ferit.slavenivic.tasky.TaskRepository;
import hr.ferit.slavenivic.tasky.model.Category;
import hr.ferit.slavenivic.tasky.model.Task;

/**
 * Created by Ivic on 4/18/2018.
 */

public class TaskViewModel extends AndroidViewModel {

    private TaskRepository mRepository;

    private LiveData<List<Task>> tasks;
    private LiveData<List<Category>> categories;

    public TaskViewModel (Application application) {
        super(application);
        mRepository = new TaskRepository(application);
        tasks = mRepository.getAllTasks();
        categories = mRepository.getAllCategories();
    }

    public LiveData<List<Task>> getAllTasks() { return tasks; }
    public LiveData<List<Category>> getAllCategories() { return categories; }

    public void insert(Task task) { mRepository.insert(task); }
    public void delete(Task task) { mRepository.delete(task); }

    public void insertCategory(Category category){ mRepository.insertCategory(category);}
    public void deleteCategory(Category category){mRepository.deleteCategory(category);}

}
