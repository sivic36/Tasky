package hr.ferit.slavenivic.tasky.room;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;
import android.os.AsyncTask;
import android.support.annotation.NonNull;

import hr.ferit.slavenivic.tasky.R;
import hr.ferit.slavenivic.tasky.model.Category;
import hr.ferit.slavenivic.tasky.model.Task;

/**
 * Created by Ivic on 4/18/2018.
 */

@Database(entities = {Task.class, Category.class}, version = 1)
public abstract class TaskRoomDatabase extends RoomDatabase {

    public abstract TaskDao taskDao();

    private static TaskRoomDatabase INSTANCE;

    public static TaskRoomDatabase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (TaskRoomDatabase.class) {
                if (INSTANCE == null) {
                    // Create database here
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            TaskRoomDatabase.class, "task_database")
                            .build();

                }
            }
        }
        return INSTANCE;
    }
}
